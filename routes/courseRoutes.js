const express = require('express');
const router = express.Router();
const courseController = require('../controllers/courseController');
const auth = require("../auth");

// Routes
// Route for creating a course
router.post("/addCourse", auth.verify, (req, res) => {

    const userRole = auth.decode(req.headers.authorization);
    console.log(userRole)
	console.log(req.headers.authorization);

    if(userRole.isAdmin === false) {
        res.send(`You do not have an access on this page.`)
    } else {
        courseController.addCourse(req.body).then(resultFromController => res.send(resultFromController))
    }
    
});

// Route for retrieving all the  courses
router.get("/all", auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization)

    courseController.getAllCourses(userData)
        .then(resultFromController => res.send(resultFromController))
});

// Route for retrieving all active courses
router.get("/", (req, res) => {

    courseController.getAllActiveCourses()
        .then(resultFromController => res.send(resultFromController))
});

// Route for retrieving a specific couurse
router.get("/:courseId", (req, res) => {
    console.log(req.params.courseId)
    courseController.getSpecificCourse(req.params)
        .then(resultFromController => res.send(resultFromController))
});

// Route for updating a course
router.put("/:courseId", auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization)

    if(userData.isAdmin) {
        courseController.updateACourse(req.params, req.body)
        .then(resultFromController => res.send(resultFromController))
    } else {
        res.send(`You do not have an access on this page.`)
    }

});

// Route for archiving a course
router.patch("/:courseId", auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization)
    console.log(userData)

    if(userData.isAdmin) {
        courseController.archiveACourse(req.params, req.body)
        .then(resultFromController => res.send(resultFromController))
    } else {
        res.send(`You do not have an access on this page.`)
    }

});

module.exports = router;

