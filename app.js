// Basic Express Server Setup
const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const userRoutes = require("./routes/userRoutes");
const courseRoutes = require("./routes/courseRoutes");

const app = express();
// ==> for alternative ports; to find other available ports in case the port 4000 is not available
const port = process.env.PORT || 4000;

// Mongoose Connection Setup
mongoose.connect(`mongodb+srv://machrispasana:admin123@224-pasana.gswcpei.mongodb.net/course-booking-API?retryWrites=true&w=majority`, 
    {
        useNewUrlParser: true,
        useUnifiedTopology: true
    }
);

let db = mongoose.connection;
db.on("error", () => console.error(`Connection Error.`));
db.once("open", () => console.error(`Yahoo! you are now connected to MongoDB! :)`))

// Middlewares
// Allows our frontend app to access our backend app.
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: true}));

// Main URI
app.use("/users", userRoutes);
app.use("/courses", courseRoutes);






app.listen(port, () => {console.log(`API is now running at port ${port}`)});