const Course = require('../models/Course');

// Controller Functions:

// Creating a new course
module.exports.addCourse = (reqBody) => {

        let newCourse = new Course ({
            name: reqBody.name, 
            description: reqBody.description, 
            price: reqBody.price
        });
        
        return newCourse.save().then((course, error) => {
            if(error) {
                return false //"Course creation failed"
            } else {
                return `You have successfully created the course!
                Course Details as follows: ${course}`
            }
        })
    
           
};

//Retrieving All Courses
module.exports.getAllCourses = (data) => {

    if(data.isAdmin) {
        return Course.find({})
            .then(result => {
                return result
            })
    } else {
        return false // "You are not an admin"
    }
};

//Retrieving All Active Courses
module.exports.getAllActiveCourses = () => {

        return Course.find({isActive: true})
            .then(result => {
                return result
            })
};

//Retrieve a specefic course
module.exports.getSpecificCourse = (reqParams) => {

    return Course.findById(reqParams.courseId)
        .then(result => {
            return result
        })
};

//Update a specefic course
module.exports.updateACourse = (reqParams, reqBody) => {

    let updatedCourse = {
        name: reqBody.name,
        description: reqBody.description,
        price: reqBody.price
    }
    return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse)
        .then((updatedCourse, error) => {

            console.log(updatedCourse)
            if(error) {
                return false
            } else {
                return true
            }
        })
};

//Archive a specefic course
module.exports.archiveACourse = (reqParams, reqBody) => {
    

    return Course.findById(reqParams.courseId).then(result => {

		console.log(reqParams.courseId);
		
		if (result == null) {
			return false
		} else {
			result.isActive = reqBody.isActive
			return result
		}
	})

        
};